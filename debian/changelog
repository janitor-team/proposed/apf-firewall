apf-firewall (9.7+rev1-6) unstable; urgency=medium

  * QA upload.
  * debian/source/format: Use "3.0 (quilt)" format.
  * debian/: Apply wrap-and-sort -abst.
  * debian/README.source: Dropped, useless.
  * debian/rules: Migrate to dh sequencer.
  * debian/dirs: Dropped, useless.
  * debian/control: Bump debhelper compat to v13.
  * debian/control: Add Vcs-* fields.
  * debian/control: Bump Standards-Version to 4.6.0.
  * debian/control: Add missing ${misc:Pre-Depends}. (lintian)

 -- Boyuan Yang <byang@debian.org>  Mon, 20 Sep 2021 20:52:10 -0400

apf-firewall (9.7+rev1-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Depend on iproute2 instead of transitional package iproute.
    (Closes: #862847)

 -- Luca Boccassi <bluca@debian.org>  Fri, 05 Jan 2018 22:54:06 +0000

apf-firewall (9.7+rev1-5) unstable; urgency=medium

  * QA upload
  * Add missing build targets

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Fri, 23 Dec 2016 23:07:23 +0100

apf-firewall (9.7+rev1-4) unstable; urgency=medium

  * QA upload
  * Set maintainer field to Debian QA Group (see #842966)
  * Make work with kernel 3.x and newer. Closes: #701674

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Wed, 21 Dec 2016 18:53:23 +0100

apf-firewall (9.7+rev1-3) unstable; urgency=low

  * [7e08452] Added iproute in Depends (Closes: #631594)
  * [a219c1e] Refreshed reserved.networks files (Closes: #627157)
  * [77f1a7c] Fixed the check_rab function (Closes: #633649)
  * [803f5fb] Bump to Standards-Version 3.9.2, no changes needed

 -- Giuseppe Iuculano <iuculano@debian.org>  Fri, 15 Jul 2011 11:54:59 +0200

apf-firewall (9.7+rev1-2) unstable; urgency=low

  * [979a674] Updated my email address
  * [a64f71f] Bump Standards-Version, no changes needed
  * [f8d719f] init: Added $network $local_fs $remote_fs in Required-Stop
  * [cc35d15] Added a README.source

 -- Giuseppe Iuculano <iuculano@debian.org>  Wed, 03 Mar 2010 15:38:09 +0100

apf-firewall (9.7+rev1-1) unstable; urgency=low

  * [2a35eda] Imported Upstream version 9.7+rev1
  * [99a4772] Refreshing patches
  * [00800d6] debian/control: Updated homepage field
  * [9e8cb5e] Updated to standards version 3.8.2 (No changes needed)
  * [b0e956e] Added wget in Depends

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Tue, 14 Jul 2009 17:46:08 +0200

apf-firewall (9.6+rev5-3) unstable; urgency=low

  * [ee70a07] Do not run cron.daily if /etc/default/apf-firewall hasn't
    RUN=yes. (Closes: #517961)

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Tue, 03 Mar 2009 21:47:15 +0100

apf-firewall (9.6+rev5-2) unstable; urgency=low

  * [a5773ac] debian/rules: Use dh_prep instead of dh_clean -k and fix
    dh-clean-k-is-deprecated lintian warning
  * [79ba2e5] debian/copyright: Use © symbol and fix copyright-with-old-
    dh-make-debian-copyright lintian warning
  * [623ba1e] debian/apf-firewall.init: Added log_end_msg to avoid bad
    formatting
  * [0c18b65] Switch to quilt
  * [ade7c24] debian/patches/01_fix_path.patch: Use /usr/sbin/apf
    instead of /etc/apf-firewall/apf
  * [928ef93] debian/rules: Do not install /etc/apf-firewall/apf
  * [cb21d95] debian/apf-firewall.logrotate: Added weekly, rotate 7, and
    compress options

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Wed, 25 Feb 2009 15:30:31 +0100

apf-firewall (9.6+rev5-1) unstable; urgency=low

  * Initial release (Closes: #495284)

 -- Giuseppe Iuculano <giuseppe@iuculano.it>  Fri, 29 Aug 2008 10:43:29 +0200

